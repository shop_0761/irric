﻿using UnityEngine;
using System.Collections;

public class ExampleScript : MonoBehaviour
{
    public Transform target;
    public Camera _camera;
    float preDist = 0.0f;
    float view = 60.0f;
    float viewMin = 10.0f;
    float viewMax = 100.0f;
    bool moveEnabled = false;

    public UnityEngine.UI.Text text;

    void Start()
    {
        text.text = moveEnabled.ToString();
    }

    public void changestate()
    {
        moveEnabled = !moveEnabled;
        target.position = Vector3.zero;
        text.text = moveEnabled.ToString();
        Camera.main.fieldOfView = 60.0f;
    }

    void Update()
    {
        // マルチタッチかどうか確認
        if (Input.touchCount >= 2)
        {
            // タッチしている２点を取得
            Touch t1 = Input.GetTouch(0);
            Touch t2 = Input.GetTouch(1);

            //2点タッチ開始時の距離を記憶
            if (t2.phase == TouchPhase.Began)
            {
                preDist = Vector2.Distance(t1.position, t2.position);
            }
            else if ((t1.phase == TouchPhase.Moved || t1.phase == TouchPhase.Stationary) &&
             (t2.phase == TouchPhase.Moved || t2.phase == TouchPhase.Stationary))
            {
                // タッチ位置の移動後、長さを再測し、前回の距離からの相対値を取る。
                float newDist = Vector2.Distance(t1.position, t2.position);
                view = view + (preDist - newDist) / 12.5f;
                preDist = newDist;
                // 限界値をオーバーした際の処理
                if (view > viewMax)
                {
                    view = viewMax;
                }
                else if (view < viewMin)
                {
                    view = viewMin;
                }
                if (moveEnabled)
                {
                    target.position = Camera.main.transform.forward * 1 * (view / 60.0f) * 2;

                }
                else
                {
                    Camera.main.fieldOfView = view;
                }


            }
        }
    }
}
