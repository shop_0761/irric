﻿namespace irric
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class GyroTest : MonoBehaviour
    {
        bool gyroEnabled = true;
         Transform _camera;

        // Use this for initialization
        void Start()
        {
            _camera = Camera.main.transform;
            Input.gyro.enabled = true;
            Screen.orientation = ScreenOrientation.LandscapeLeft;
        }

        // Update is called once per frame
        void Update()
        {
            if (gyroEnabled)
            {
                _camera.rotation = Quaternion.AngleAxis(90.0f, Vector3.right) * Input.gyro.attitude * Quaternion.AngleAxis(180.0f, Vector3.forward);
            }

        }
    }


}