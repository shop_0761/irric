﻿namespace irric
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;

    [RequireComponent(typeof(Button))]
    public class ChangePhoto : MonoBehaviour
    {
        public string sphereName = "PanoramaSphere";
        GameObject panoSphere;

        Material mat;
        public Texture2D woodenTex;
        public Texture2D rcTex;
        public bool isWood = true;
        Gyro gyro;

        void Start()
        {
            panoSphere = GameObject.Find(sphereName);
            gameObject.GetComponent<Button>().onClick.AddListener(() => SetPhoto());
            gyro = Camera.main.GetComponent<Gyro>();
        }

        public void SetPhoto()
        {
            if (!mat)
            {
                mat = panoSphere.GetComponent<Renderer>().material;
            }

            if (woodenTex && rcTex)
            {
                mat.SetTexture("_MainTex", isWood ? woodenTex : rcTex);
                PinchInOut.Reset();
                gyro.Reset();

                //管理してるUIに通知
                ExecuteEvents.Execute<RecieveBtnInterface>(
                                                target: transform.parent.gameObject,
                                                eventData: null,
                                                functor: (reciever, eventData) => reciever.OnClickedBtn(gameObject)
                                                );
            }

        }

    }

}