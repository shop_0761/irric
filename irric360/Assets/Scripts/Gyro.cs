﻿namespace irric
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class Gyro : MonoBehaviour
    {

        private Quaternion gyroini;
        private Quaternion gyro;

        bool gyroEnabled = false;
        public float _rotSpeed = 60.0f;
        Vector2 _prevPos = Vector2.zero;

        public Text[] gyroText;
        Transform _camera;

        public Text test;

        float vertical;
        float horizontal;
        float preHorizontal;
        public GameObject panoSphere;
        public GvrHead gvrHead;

        // Use this for initialization
        void Start()
        {
            _camera = Camera.main.transform;
            //Input.gyro.enabled = true;
            gvrHead.DisableGyro();
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            horizontal = 0;
            vertical = 0;

            foreach (var item in gyroText)
            {
                item.text = gyroEnabled ? "Gyro" : "Swipe";
            }
        }


        public void Reset()
        {
            panoSphere.transform.rotation = gyroEnabled ? Quaternion.AngleAxis(_camera.rotation.eulerAngles.y, Vector3.up) : Quaternion.identity;

            horizontal = 0;
            vertical = 0;
            _camera.rotation = Quaternion.Euler(vertical, horizontal, 0);
        }

        public void ChangeMode()
        {

            gyroEnabled = !gyroEnabled;
            if (gyroEnabled)
            {
                gvrHead.EnableGyro();
            }
            else
            {
                gvrHead.DisableGyro();
            }

            foreach (var item in gyroText)
            {
                item.text = gyroEnabled ? "Gyro" : "Swipe";
            }

            if (gyroEnabled)
            {
                //gvrHead.Recenter();
                //preHorizontal = (Quaternion.AngleAxis(90.0f, Vector3.right) * Input.gyro.attitude * Quaternion.AngleAxis(180.0f, Vector3.forward)).eulerAngles.y;
                panoSphere.transform.rotation *= Quaternion.Euler(0, preHorizontal - horizontal, 0);

            }

            else
            {
                preHorizontal = _camera.rotation.eulerAngles.y;
                horizontal = 0;
                vertical = 0;
            }
        }


        // Update is called once per frame
        void Update()
        {
            test.text = "preH: " + preHorizontal + " H: " + horizontal + " preH - H:" + (preHorizontal - horizontal) + "pSpheRot: " + panoSphere.transform.rotation.eulerAngles.y;
            if (Input.touchCount == 1 && !PinchInOut.pinching)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    _prevPos = touch.position;
                }
                else
                {
                    Vector2 delta = touch.position - _prevPos;
                    _prevPos = touch.position;

                    horizontal = _camera.eulerAngles.y + (delta.x / Screen.width) * -_rotSpeed;
                    vertical = _camera.eulerAngles.x + (delta.y / Screen.height) * _rotSpeed;

                    _camera.rotation = Quaternion.Euler(vertical, horizontal, 0);
                }

            }
        }
    }


}