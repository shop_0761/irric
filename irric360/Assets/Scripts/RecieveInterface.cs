﻿namespace irric
{
    using UnityEngine;
    using UnityEngine.EventSystems;

    public interface RecieveBtnInterface : IEventSystemHandler
    {
        void OnClickedBtn(GameObject obj);
    }

    public interface RecieveHomeBtnInterface : IEventSystemHandler
    {
		void OnRecieveGoBackHome(GameObject obj);
    }

}