﻿namespace irric
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;

    public class GoBackHome : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {
            gameObject.GetComponent<Button>().onClick.AddListener(() => GoBack());
        }

        void GoBack()
        {
            ExecuteEvents.Execute<RecieveHomeBtnInterface>(
                                                            target: transform.root.gameObject,
                                                            eventData: null,
                                                            functor: (reciever, eventData) => reciever.OnRecieveGoBackHome(gameObject)
                                                     );
        }

    }

}