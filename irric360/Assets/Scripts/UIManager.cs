﻿namespace irric
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class UIManager : MonoBehaviour, RecieveBtnInterface, RecieveHomeBtnInterface
    {
        List<Canvas> UIs = new List<Canvas>();

        public Image logo;
        public float fadeInTime = 2.0f;

        // Use this for initialization
        void Start()
        {
            foreach (Transform item in transform)
            {
                Canvas cv = item.GetComponent<Canvas>();
                UIs.Add(cv);
                cv.enabled = false;

            }

            StartCoroutine(FadeAction());
        }


        IEnumerator FadeAction()
        {
			UIs[0].enabled = true;
			ChangeVisible(UIs[0].transform, false);

            yield return StartCoroutine(FadeIn());
            yield return new WaitForSeconds(1.0f);

			ChangeVisible(UIs[0].transform, true);
        }

		void ChangeVisible(Transform tf, bool b){
			foreach (Transform item in tf)
			{
				if(item.name.Contains("Logo") || item.name.Contains("White")){
					continue;
				}

				else{
					item.gameObject.SetActive(b);
				}
			}
		}

        IEnumerator FadeIn()
        {
            float alpha = 0;
            float delta = 1.0f / fadeInTime * Time.deltaTime;
            while (alpha < 1.0f && logo)
            {
                alpha += delta;
                logo.color = new Color(1, 1, 1, alpha);
                yield return null;
            }
        }

        //Home UI の UI Controllerから
        public void OnClickedBtn(GameObject obj)
        {
            UIs[0].enabled = false;

            var homeUI = obj.GetComponent<UIController>();
            var index = homeUI.isOutside ? 1 : 2;
            UIs[index].enabled = true;
            var targetUI = UIs[index].gameObject.GetComponent<UIController>();

            targetUI.Reset();
            targetUI.ChangeWoodState(homeUI.woodState);
            targetUI.modeState.text = (homeUI.woodState ? "木造" : "非木造") + (homeUI.isOutside ? "外観" : "内観");

        }

        //homeボタンから
        public void OnRecieveGoBackHome(GameObject obj)
        {
            int index = UIs.IndexOf(obj.transform.parent.gameObject.GetComponent<Canvas>());
            UIs[index].enabled = false;

            StartCoroutine(FadeAction());
        }
    }


}