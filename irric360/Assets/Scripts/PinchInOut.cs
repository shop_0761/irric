﻿namespace irric
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class PinchInOut : MonoBehaviour
    {
        //カメラ視覚の範囲
        float viewMin = 10.0f;
        float viewMax = 100.0f;

        //直前の2点間の距離.
        private float preDist = 0.0f;
        //初期値
        static float view = 60.0f;

        public static bool pinching = false;

        public Text test;
        
        public static void Reset()
        {
            Camera.main.fieldOfView = 60.0f;
            view = 60.0f;

        }
        

        // Update is called once per frame
        void Update()
        {
            if(Input.touchCount == 1){
                Touch t1 = Input.GetTouch(0);
                if(pinching && t1.phase == TouchPhase.Ended){
                    pinching = false;
                }
            }

            // マルチタッチかどうか確認
            else if (Input.touchCount >= 2)
            {
                // タッチしている２点を取得
                Touch t1 = Input.GetTouch(0);
                Touch t2 = Input.GetTouch(1);

                //2点タッチ開始時の距離を記憶
                if (t2.phase == TouchPhase.Began)
                {
                    preDist = Vector2.Distance(t1.position, t2.position);
                }
                else if ((t1.phase == TouchPhase.Moved || t1.phase == TouchPhase.Stationary) &&
                 (t2.phase == TouchPhase.Moved || t2.phase == TouchPhase.Stationary))
                {
                    // タッチ位置の移動後、長さを再測し、前回の距離からの相対値を取る。
                    float newDist = Vector2.Distance(t1.position, t2.position);
                    view = view + (preDist - newDist) / 12.5f;
                    preDist = newDist;
                    // 限界値をオーバーした際の処理
                    if (view > viewMax)
                    {
                        view = viewMax;
                    }
                    else if (view < viewMin)
                    {
                        view = viewMin;
                    }

                    Camera.main.fieldOfView = view;
                    pinching = true;
                }

                else if(t1.phase == TouchPhase.Ended && t2.phase == TouchPhase.Ended){
                    pinching = false;
                }
            }
        }

    }
}