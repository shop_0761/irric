﻿namespace irric
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class UIController : MonoBehaviour, RecieveBtnInterface
    {

        List<GameObject> child = new List<GameObject>();
        Image currentBtnImg;
        [HideInInspector]
        public bool woodState = true;
        [HideInInspector]
        public bool isOutside = true; 
        [HideInInspector]
        public Text modeState;
        [HideInInspector]
        public GameObject latestClickedObj;
        public Color highLightColor = new Color(1f, 1f, 0.5f, 1f);

        void Start()
        {
            foreach (Transform item in transform)
            {
                if (item.name.Contains("White")) continue;
                child.Add(item.gameObject);
                
                Text txt = item.GetComponent<Text>();
                if (txt)
                {
                    modeState = txt;
                }
            }

            Reset();
        }

        public void Reset()
        {
            if (this.name.Contains("Home")) return;


            if (currentBtnImg)
            {
                currentBtnImg.color = Color.white;
            }
            currentBtnImg = child[0].GetComponent<Image>();
            currentBtnImg.color = highLightColor;

        }

        public void ChangeWoodState(bool isWood)
        {

            foreach (var item in child)
            {
                var cp = item.GetComponent<ChangePhoto>();
                if (cp)
                {
                    cp.isWood = isWood;
                }
            }
        }

        public void OnClickedBtn(GameObject obj)
        {

            if (obj.transform.parent.gameObject.name.Contains("Home"))
            {
                var cp = obj.GetComponent<ChangePhoto>();
                woodState = cp.isWood;
                isOutside = obj.name.Contains("Outside") ? true : false;
                ExecuteEvents.Execute<RecieveBtnInterface>(
                                                target: transform.parent.gameObject,
                                                eventData: null,
                                                functor: (reciever, eventData) => reciever.OnClickedBtn(gameObject)
                                                ); 
            }

            else
            {
                currentBtnImg.color = Color.white;
                currentBtnImg = obj.GetComponent<Image>();
                currentBtnImg.color = highLightColor;
            }
        }

    }


}